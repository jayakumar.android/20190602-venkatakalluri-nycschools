package com.nycschools.nyc

import android.support.v4.app.Fragment

/**
 * <b>Mandatory extension for all the Fragments in the application.
 *
 * <p>Provides necessary functions that act as prerequisite operations for every Fragment.
 */
abstract class BaseFragment : Fragment() {

    /**
     * Initializes DataBinding and ViewModel objects.
     */
    abstract fun initBindingAndViewModel()

    /**
     * Sets up the observers from ViewModel to observe changes in data.
     */
    abstract fun setupObservers()


}