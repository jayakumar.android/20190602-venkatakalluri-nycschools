package com.nycschools.nyc.models

data class School(val dbn: String, val school_name: String) {
}
