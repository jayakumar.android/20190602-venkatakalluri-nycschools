package com.nycschools.nyc.models

/**
 * immutable class for SchoolDetails
 * @param dbn schoolid
 * @param school_name school name
 * @param num_of_sat_test_takers Num of SAT test takers
 * @param sat_critical_reading_avg_score SAT critical reading Avg score
 * @param sat_math_avg_score SAT MAT Avg Score
 * @param sat_writing_avg_score SAT Writing Avg score
 */
data class SchoolDetails(val dbn: String,
                         val school_name: String?=null,
                         val num_of_sat_test_takers: String?=null,
                         val sat_critical_reading_avg_score: String?=null,
                         val sat_math_avg_score: String?=null,
                         val sat_writing_avg_score: String? = null) {


    override fun equals(other: Any?): Boolean {

        var temp =other as SchoolDetails

        return this.dbn.equals(temp.dbn)
    }

}