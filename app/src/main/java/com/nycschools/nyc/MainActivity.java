package com.nycschools.nyc;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.nycschools.nyc.feature.SchoolDetailFragment;
import com.nycschools.nyc.feature.SchoolsFragment;
import com.nycschools.nyc.models.SchoolDetails;

/**
 * Entry screen for the application
 * it handles the fragments of schools feature
 * note : only targets mobile
 */

public class MainActivity extends AppCompatActivity {


    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();

        if(savedInstanceState==null){
            addSchoolsFragment();
        }


    }

    //it adds schools list fragment
    public void addSchoolsFragment(){
        mFragmentTransaction=mFragmentManager.beginTransaction();
        SchoolsFragment countryListFragment=new SchoolsFragment();
        mFragmentTransaction.add(R.id.fragmentContainer,countryListFragment);
        mFragmentTransaction.commit();
    }


    /*
    it adds the school details fragmennt on top of schools list fragment
     it calls from [SchoolsFragment]
    */
      public void addSchoolDetailFragment(String dbn){
        mFragmentTransaction=mFragmentManager.beginTransaction();

        SchoolDetailFragment schoolDetailFragment=new SchoolDetailFragment();
        Bundle bundle=new Bundle();
        bundle.putString("dbn",dbn);
        schoolDetailFragment.setArguments(bundle);

        mFragmentTransaction.add(R.id.fragmentContainer,schoolDetailFragment);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }



}
