package com.nycschools.nyc.feature

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nycschools.nyc.BaseFragment
import com.nycschools.nyc.MainActivity
import com.nycschools.nyc.R
import com.nycschools.nyc.databinding.FragmentSchoolsBinding
import com.nycschools.nyc.models.School

/**
 * Displays the schools list
 *
 */
class SchoolsFragment : BaseFragment() {


    private lateinit var mActivity: MainActivity
    private lateinit var mBinding: FragmentSchoolsBinding
    private lateinit var mViewModel: SchoolsViewModel;
    private lateinit var mAdapter: SchoolsAdapter
    private var mSchoolsList: List<School> = ArrayList()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mActivity = context as MainActivity
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        mBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_schools,
                container,
                false
        )
        return mBinding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initBindingAndViewModel()
        setupObservers()
        initRecyclerView()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        savedInstanceState?.let {

            mActivity = activity as MainActivity

        }
    }

    /*
     * Setting up view-model
     *
     */
    override fun initBindingAndViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SchoolsViewModel::class.java)
        mBinding.setLifecycleOwner(this)
        mBinding.viewModel = mViewModel
        mBinding.executePendingBindings()
    }

    /*
      setup the recyclerview
     */

    private fun initRecyclerView() {
        mAdapter = SchoolsAdapter(mViewModel)
        mBinding.rvSchoolsList.layoutManager = LinearLayoutManager(context)
        //mBinding.rvSchoolsList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        mBinding.rvSchoolsList.adapter = mAdapter
    }

    override fun setupObservers() {
        mViewModel.getSchoolsList(mViewModel.mRepo!!)

        mViewModel.mObsSchoolsList.observe(mActivity, Observer {
            mSchoolsList = it as List<School>
            if (mSchoolsList.isNotEmpty()) {
                mAdapter.setSchoolsList(mSchoolsList)
                mBinding.rvSchoolsList.scrollToPosition(0)

            }
        })


        mViewModel.mObsSelectedSchool?.observe(mActivity, Observer {

            var temp = it;
            temp?.let{
            mViewModel.mObsSelectedSchool?.let {


                    mActivity.addSchoolDetailFragment(temp?.dbn)
                    mViewModel.mObsSelectedSchool.value = null;

                }

            }
        })


    }

    companion object {
        val TAG = SchoolsFragment::class.java.simpleName
    }


}