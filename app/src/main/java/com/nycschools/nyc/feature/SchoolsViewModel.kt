package com.nycschools.nyc.feature

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Context
import com.nycschools.nyc.common.utlIsNetworkAvailable
import com.nycschools.nyc.models.School
import com.nycschools.nyc.models.SchoolDetails
import com.nycschools.nyc.repo.SchoolRepo
import com.nycschools.nyc.sources.RetrofitDataSource
import com.nycschools.nyc.sources.SchoolDataSource


/**
 * Exposes the data to be used in schools list screen and school detail screnn
 *
 * implements a listener registration mechanism which is notified when propery changes
 *
 */
class SchoolsViewModel(application: Application) : AndroidViewModel(application), SchoolsListHandler {


    var mObsSelectedSchool: MutableLiveData<School> = MutableLiveData() //observable for selected school

    val mObsSchoolsList: MutableLiveData<List<School>> = MutableLiveData(); // observable for schools list

    val mObsSelectedSchoolDetails: MutableLiveData<SchoolDetails> = MutableLiveData() //observable for school details

    val mObsIsDataExist: MutableLiveData<Boolean> = MutableLiveData(); //observable for no data message
    val mObsIsConnectionExist: MutableLiveData<Boolean> = MutableLiveData(); //observable for internet connection
    val mObsIsShowProgress: MutableLiveData<Boolean> = MutableLiveData(); //observable for progressbar

     var mRepo: SchoolRepo? = null;

    private var mContext: Context? = null


    init {

        mContext = application.applicationContext
        //initialze repo
        mRepo = SchoolRepo(RetrofitDataSource(application.applicationContext))
    }


    //fetches the schools list
    fun getSchoolsList(repo: SchoolRepo) {

       if (utlIsNetworkAvailable(mContext!!)) {
           mObsIsConnectionExist.value = true
            fetchSchoolList(repo)
        } else {

            mObsIsConnectionExist.value = false

        }

    }

    fun fetchSchoolList(repo: SchoolRepo) {
        mObsIsShowProgress.value = true
        repo!!.getSchools(object : SchoolDataSource.SchoolListCallback {
            override fun onSchoolsListReceived(schools: List<School>) {
                mObsSchoolsList.value = schools;
                mObsIsShowProgress.value = false
            }

            override fun onFailure(error_code: Int, reason: String) {
                mObsIsShowProgress.value = false
            }


        })
    }

    /* it fetches the detials for school detail screen
     *@param dbn  id to fetch school details
     */

    fun getSchoolsDetails(repo: SchoolRepo, dbn: String) {

        if (utlIsNetworkAvailable(mContext!!)) {
            mObsIsConnectionExist.value = true
            fetchSchoolDetails(repo, dbn)

        } else {
            mObsIsConnectionExist.value = false

        }
    }

    fun fetchSchoolDetails(repo: SchoolRepo, dbn: String) {
        mObsIsShowProgress.value = true
        repo!!.getSchoolsDetails(object : SchoolDataSource.SchoolDetailsListCallback {
            override fun onSchoolsDetailsListReceived(schools: List<SchoolDetails>) {

                mObsIsShowProgress.value = false
                var tempData = SchoolDetails(dbn);

                var selectedData = findSelctedDetails(tempData, schools)


                // if no data found, shows message otherwise display school details
                if (selectedData != null) {
                    mObsSelectedSchoolDetails.value = selectedData;
                    mObsIsDataExist.value = true
                } else {

                    mObsIsDataExist.value = false
                }


            }

            override fun onFailure(error_code: Int, reason: String) {
                mObsIsShowProgress.value = false
            }


        })
    }

    /*
     * it compares the school id in the list and returns the data otherwise null
     *
     * @param selectedData user selected one
     * @param schools   total school data
     */

    fun findSelctedDetails(selectedData: SchoolDetails, schools: List<SchoolDetails>): SchoolDetails? {


        var selectedIndex = schools.indexOf(selectedData)

        if (selectedIndex > -1) {

            return schools.get(selectedIndex)
        }

        return null;

    }

    /*
     * called by list item click [item_schools_list.xml]
     */
    override fun onSchoolSelected(school: School) {

        mObsSelectedSchool?.value = school;

    }


}