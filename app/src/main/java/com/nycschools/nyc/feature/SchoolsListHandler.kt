package com.nycschools.nyc.feature

import com.nycschools.nyc.models.School

/**
 * selected school data
 *
 */

interface SchoolsListHandler {

    fun onSchoolSelected(school: School)

}