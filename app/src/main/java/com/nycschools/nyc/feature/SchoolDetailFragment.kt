package com.nycschools.nyc.feature

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nycschools.nyc.BaseFragment
import com.nycschools.nyc.MainActivity
import com.nycschools.nyc.R
import com.nycschools.nyc.databinding.FragmentSchoolDetailBinding


/**
 * it shows the school details
 *
 */
class SchoolDetailFragment : BaseFragment() {

    private lateinit var mActivity: MainActivity
    private lateinit var mBinding: FragmentSchoolDetailBinding
    private lateinit var mViewModel: SchoolsViewModel;

    private var dbn: String? = null
    private var ARG_PARAM1: String? = "dbn"


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mActivity = context as MainActivity
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dbn = it.getString(ARG_PARAM1)

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_school_detail,
                container,
                false
        )
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initBindingAndViewModel()
        setupObservers()
    }


    override fun initBindingAndViewModel() {
        mViewModel = ViewModelProviders.of(this).get(SchoolsViewModel::class.java)
        mBinding.viewModel=mViewModel
        mBinding.setLifecycleOwner(this)
        mBinding.executePendingBindings()
    }

    override fun setupObservers() {

        mViewModel.getSchoolsDetails(mViewModel.mRepo!!,dbn!!)

        mViewModel.mObsSelectedSchoolDetails.observe(mActivity, Observer {


            if(mViewModel.mObsSelectedSchoolDetails.value !=null)
            {
               it?.let {

                    mBinding.schoolDetail = it;

                    mViewModel.mObsSelectedSchoolDetails.value = null
                }

            }


        })

    }

}
