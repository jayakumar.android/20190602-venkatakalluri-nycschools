package com.nycschools.nyc.feature;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.nycschools.nyc.databinding.ItemSchoolsListBinding;
import com.nycschools.nyc.models.School;

import java.util.ArrayList;
import java.util.List;


/**
 * Adapter for schools data
 */

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolViewHolder>{


    private List<School> mSchoolsList = new ArrayList<>();
    private SchoolsListHandler mHandler;

    public SchoolsAdapter(SchoolsListHandler handler) {
        this.mHandler = handler;
    }

    public void setSchoolsList(List<School> schoolsList) {
        this.mSchoolsList = schoolsList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        //inflate
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        //Create the binding
        ItemSchoolsListBinding binding = ItemSchoolsListBinding.inflate(layoutInflater, parent, false);
        return new SchoolViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder schoolViewHolder, int position) {
        schoolViewHolder.bind(mSchoolsList.get(position));

    }


    @Override
    public int getItemCount() {
        return mSchoolsList.size();
    }

      //view holder class
    public class SchoolViewHolder extends RecyclerView.ViewHolder{


        private ItemSchoolsListBinding mItemSchoolsListBinding;

        public SchoolViewHolder(ItemSchoolsListBinding schoolsListBinding) {
            super(schoolsListBinding.getRoot());

            this.mItemSchoolsListBinding = schoolsListBinding;
        }


        public void bind(School school){

            mItemSchoolsListBinding.setSchool(school);
            mItemSchoolsListBinding.setHandler(mHandler);


        }


    }




}
