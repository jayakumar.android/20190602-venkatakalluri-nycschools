package com.nycschools.nyc.common

import android.content.Context
import android.net.ConnectivityManager


/**
 * Contains all the validation functions that are to be used throughout the application.
 */


/**
 * Method to check whether internet connection is available, and return a [Boolean]
 * based on the result.
 */
fun utlIsNetworkAvailable(context: Context): Boolean {

    val connectivityManager = context.getSystemService(
            Context.CONNECTIVITY_SERVICE
    ) as? ConnectivityManager

    val activeNetworkInfo = connectivityManager?.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}