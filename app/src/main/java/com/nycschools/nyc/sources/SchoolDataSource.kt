package com.nycschools.nyc.sources

import android.util.Log
import com.nycschools.nyc.models.School
import com.nycschools.nyc.models.SchoolDetails

/**
 *
 * Main entry point for accessing schools data
 *
 *  getSchools and getSchoolsDetails have callbacks. Consider adding callbacks to other
 * methods to inform the user of network errors or successful operations
 */

interface SchoolDataSource {

    fun getSchools(callback: SchoolListCallback) {

        Log.d(TAG, "Default Implementation")
    }

    fun getSchoolsDetails(callback: SchoolDetailsListCallback) {


        Log.d(TAG, "Default Implementation")
    }

    interface SchoolListCallback {

        fun onSchoolsListReceived(schools: List<School>)

        fun onFailure(error_code: Int, reason: String)

    }



    interface SchoolDetailsListCallback {

        fun onSchoolsDetailsListReceived(schools: List<SchoolDetails>)
        fun onFailure(error_code: Int, reason: String)
    }


    companion object {

        val TAG = SchoolDataSource::class.java.simpleName


    }

}