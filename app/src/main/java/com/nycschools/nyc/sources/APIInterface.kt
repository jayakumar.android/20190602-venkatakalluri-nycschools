package com.nycschools.nyc.sources

import com.nycschools.nyc.models.School
import com.nycschools.nyc.models.SchoolDetails
import retrofit2.Call
import retrofit2.http.GET

/**
 * define required api calls
 *
 */

interface APIInterface {


    @GET("/resource/s3k6-pzi2.json")
    fun getSchools(): Call<List<School>>

    @GET("/resource/f9bf-2cp4.json")
    fun getSchoolsDetails(): Call<List<SchoolDetails>>
}