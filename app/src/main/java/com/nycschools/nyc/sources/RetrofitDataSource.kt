package com.nycschools.nyc.sources

import android.content.Context
import com.google.gson.GsonBuilder
import com.nycschools.nyc.R
import com.nycschools.nyc.models.School
import com.nycschools.nyc.models.SchoolDetails
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * - it uses retrofit for api calls
 * - it requires context the initialize the objects
 */

class RetrofitDataSource(context: Context) : SchoolDataSource {

    //baseurl
    private val baseUrl = context.getString(R.string.base_url)

    //interceptor api calls
    private var client = OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    private var gson = GsonBuilder().create()  //for serialization/deserialization

    //build the retrofit object
    private var retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

    private var retrofitService = createService()


    /*
       it creates the retrofit object which requires API interface
     */
    private fun createService(): APIInterface {
        return retrofit.create(APIInterface::class.java)

    }

    override fun getSchools(callback: SchoolDataSource.SchoolListCallback) {
        var call = retrofitService.getSchools()

        call.enqueue(object : retrofit2.Callback<List<School>> {
            override fun onFailure(call: Call<List<School>>?, t: Throwable?) {

                callback.onFailure(401, "failure") //

            }

            override fun onResponse(call: Call<List<School>>?, response: Response<List<School>>?) {

                if (response?.body() != null) {
                    callback.onSchoolsListReceived(response!!.body())
                } else {

                    callback.onFailure(401, "server not found") //TODO: handle error scenario
                }
            }


        })

    }

    override fun getSchoolsDetails(callback: SchoolDataSource.SchoolDetailsListCallback) {
        var call = retrofitService.getSchoolsDetails()
        call.enqueue(object : retrofit2.Callback<List<SchoolDetails>> {
            override fun onFailure(call: Call<List<SchoolDetails>>?, t: Throwable?) {

                callback.onFailure(401, "failure")

            }

            override fun onResponse(call: Call<List<SchoolDetails>>?, response: Response<List<SchoolDetails>>?) {
                if (response?.body() != null) {

                    callback.onSchoolsDetailsListReceived(response!!.body())
                } else {

                    callback.onFailure(401, "server not found") //TODO:handle error scenario
                }
            }


        })
    }


    companion object {
        val TAG = RetrofitDataSource::class.java
    }
}