package com.nycschools.nyc.repo

import android.content.Context
import com.nycschools.nyc.sources.RetrofitDataSource
import com.nycschools.nyc.sources.SchoolDataSource


/**
 *
 * implementation for fetching schools data
 *
 * In general repo to naviagte the requests to multiple datasources
 *
 * Currenly one datasource is exist which is retrofit.
 *
 */
class SchoolRepo(dataSource: RetrofitDataSource):SchoolDataSource {

    var mDataSource: RetrofitDataSource = dataSource
    override fun getSchools(callback: SchoolDataSource.SchoolListCallback) {
        mDataSource.getSchools(callback)
    }

    override fun getSchoolsDetails(callback: SchoolDataSource.SchoolDetailsListCallback) {
        mDataSource.getSchoolsDetails(callback)
    }

}