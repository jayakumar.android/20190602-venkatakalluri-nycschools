package com.nycschools.nyc
import com.nycschools.nyc.models.SchoolDetails
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * unit test cases for [SchoolDetailsTest]
 */
class SchoolDetailsTest {

    private lateinit var schoolDetails: SchoolDetails
    private lateinit var schoolDetails2: SchoolDetails

    @Before
    @Throws(Exception::class)
    fun setUp() {
        schoolDetails = SchoolDetails("01M292", "NAV")
        schoolDetails2 = SchoolDetails("01M292", "NAV","22","33","45","67")
    }


    @Test
    @Throws(Exception::class)
    fun testSchoolDetails(){
        Assert.assertEquals(EXPECTED_DBN, schoolDetails.dbn);
        Assert.assertEquals(EXPECTED_SCHOOL_NAME, schoolDetails.school_name);
        Assert.assertNull(schoolDetails.num_of_sat_test_takers)
        Assert.assertNull(schoolDetails.sat_critical_reading_avg_score)
        Assert.assertNull(schoolDetails.sat_math_avg_score)
        Assert.assertNull(schoolDetails.sat_writing_avg_score)

    }

    @Test
    @Throws(Exception::class)
    fun testSchoolDetails_withFullDATA(){
        Assert.assertEquals(EXPECTED_DBN, schoolDetails2.dbn);
        Assert.assertEquals(EXPECTED_SCHOOL_NAME, schoolDetails2.school_name);
        Assert.assertEquals(EXPECTED_NO_OF_TEST_MAKERS, schoolDetails2.num_of_sat_test_takers);
        Assert.assertEquals(EXPECTED_CRITICAL_READING_AVG_SCORE, schoolDetails2.sat_critical_reading_avg_score);
        Assert.assertEquals(EXPECTED_MATH_AVG_SCORE, schoolDetails2.sat_math_avg_score);
        Assert.assertEquals(EXPECTED_WRITE_AVG_SCORE, schoolDetails2.sat_writing_avg_score);

    }

    companion object{

        var EXPECTED_DBN : String = "01M292"
        var EXPECTED_SCHOOL_NAME : String = "NAV"
        var EXPECTED_NO_OF_TEST_MAKERS : String = "22"
        var EXPECTED_CRITICAL_READING_AVG_SCORE: String = "33"
        var EXPECTED_MATH_AVG_SCORE: String = "45"
        var EXPECTED_WRITE_AVG_SCORE : String = "67"

    }

}