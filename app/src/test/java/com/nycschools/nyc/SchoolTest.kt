package com.nycschools.nyc

import com.nycschools.nyc.models.School
import org.junit.Assert
import org.junit.Before
import org.junit.Test


/**
 * unit test cases for [School]
 */
class SchoolTest {

    private lateinit var school: School

    @Before
    @Throws(Exception::class)
    fun setUp() {
        school = School("01M292", "NAV")
    }


    @Test
    @Throws(Exception::class)
    fun testSchoolDetails(){
        Assert.assertEquals(EXPECTED_DBN, school.dbn);
        Assert.assertEquals(EXPECTED_SCHOOL_NAME, school.school_name);

    }

    companion object{

        var EXPECTED_DBN : String = "01M292"
        var EXPECTED_SCHOOL_NAME : String = "NAV"

    }

}