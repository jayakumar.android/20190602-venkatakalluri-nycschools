package com.nycschools.nyc

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
        SchoolTest::class,
        SchoolDetailsTest::class,
        SchoolViewModelTest::class
)
class MainSuite{

}