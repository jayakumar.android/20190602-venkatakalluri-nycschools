package com.nycschools.nyc

import android.app.Application
import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.content.Context
import android.content.res.Resources
import com.nhaarman.mockitokotlin2.capture
import com.nycschools.nyc.common.utlIsNetworkAvailable
import com.nycschools.nyc.feature.SchoolsViewModel
import com.nycschools.nyc.models.School
import com.nycschools.nyc.models.SchoolDetails
import com.nycschools.nyc.repo.SchoolRepo
import com.nycschools.nyc.sources.RetrofitDataSource
import com.nycschools.nyc.sources.SchoolDataSource
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*
import kotlin.collections.arrayListOf as arrayListOf1

/**
 * Unit tests for the implementaton of [SchoolsViewModel]
 */
class SchoolViewModelTest {

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var schoolsRepostiory: SchoolRepo
    @Mock private lateinit var context: Application

    @Captor
    private lateinit var loadSchoolsCallbackCaptor: ArgumentCaptor<SchoolDataSource.SchoolListCallback>


    @Captor
    private lateinit var loadSchoolDetailsCallbackCaptor: ArgumentCaptor<SchoolDataSource.SchoolDetailsListCallback>

    private lateinit var schoolsViewModel: SchoolsViewModel

    private lateinit var schoolsList: List<School>
    private lateinit var schoolsDetailsList: List<SchoolDetails>

    private var selectedDbn: String = "01M292"
    private var selectedDbn2: String = "01M2930"// no data exist for this id

    @Before
    fun setupSchoolsViewModel(){

        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)


        setupContext()



        // Get a reference to the class under test
        schoolsViewModel = SchoolsViewModel(context)


        val school1 = School("01M292","HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        val school2 = School("01M448","UNIVERSITY NEIGHBORHOOD HIGH SCHOOL")
        val school3 = School("01M450","EAST SIDE COMMUNITY SCHOOL")


        //initialize the schoolsLiss
        schoolsList = arrayListOf1(school1,school2,school3)



        val schoolDetails1 = SchoolDetails("01M292","HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES","22","23","34","35")
        val schoolDetails2 = SchoolDetails("01M448","UNIVERSITY NEIGHBORHOOD HIGH SCHOOL","32","33","345","356")
        val schoolDetails3 = SchoolDetails("01M450","EAST SIDE COMMUNITY SCHOOL","332","23","35","45")


        schoolsDetailsList = arrayListOf1(schoolDetails1,schoolDetails2,schoolDetails3)

    }


    private fun setupContext(){

        `when`<Context>(context.applicationContext).thenReturn(context)
        `when`(context.getString(R.string.base_url))
                .thenReturn("https://data.cityofnewyork.us/")
        `when`(context.resources).thenReturn(mock(Resources::class.java))
        `when`(utlIsNetworkAvailable(context)).thenReturn(true)



    }

    @Test
    fun loadSchoolsFromRepository_dataLoaded(){

        //Given an initialized SchoolViewModel with intialized schools
        // loading of schools required
        schoolsViewModel.fetchSchoolList(schoolsRepostiory)

        verify<SchoolRepo>(schoolsRepostiory).getSchools(capture(loadSchoolsCallbackCaptor))

        //Then progress indicator is shown
        assertTrue(LiveDataTestUtil.getValue(schoolsViewModel.mObsIsShowProgress))
        loadSchoolsCallbackCaptor.value.onSchoolsListReceived(schoolsList)


        //progress bar is hidden
        assertFalse(LiveDataTestUtil.getValue(schoolsViewModel.mObsIsShowProgress))


        //data is loaded
        assertFalse(LiveDataTestUtil.getValue(schoolsViewModel.mObsSchoolsList).isEmpty())
        assertTrue(LiveDataTestUtil.getValue(schoolsViewModel.mObsSchoolsList).size == 3)


    }

    @Test
    fun checkSchoolDetails_data(){

        //Given an initialized SchoolViewModel with intialized schools
        // loading of schools required
        schoolsViewModel.fetchSchoolDetails(schoolsRepostiory,selectedDbn)

        // Callback is captured and invoked with stubbed tasks
        verify<SchoolRepo>(schoolsRepostiory).getSchoolsDetails(capture(loadSchoolDetailsCallbackCaptor))

        //Then progress indicator is shown
        assertTrue(LiveDataTestUtil.getValue(schoolsViewModel.mObsIsShowProgress))
        loadSchoolDetailsCallbackCaptor.value.onSchoolsDetailsListReceived(schoolsDetailsList)


        //progress bar is hidden
        assertFalse(LiveDataTestUtil.getValue(schoolsViewModel.mObsIsShowProgress))


        //to verify findSchoolDetails method
        assertTrue(LiveDataTestUtil.getValue(schoolsViewModel.mObsSelectedSchoolDetails).school_name.equals("HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"))
        assertFalse(LiveDataTestUtil.getValue(schoolsViewModel.mObsSelectedSchoolDetails).school_name.equals("EAST SIDE COMMUNITY SCHOOL"))


    }

    @Test
    fun noDataSecanarioOfSchoolDetails_data(){

        //Given an initialized SchoolViewModel with intialized schools
        // loading of schools required
        schoolsViewModel.fetchSchoolDetails(schoolsRepostiory,selectedDbn2)

        // Callback is captured and invoked with stubbed tasks
        verify<SchoolRepo>(schoolsRepostiory).getSchoolsDetails(capture(loadSchoolDetailsCallbackCaptor))

        //Then progress indicator is shown
        assertTrue(LiveDataTestUtil.getValue(schoolsViewModel.mObsIsShowProgress))
        loadSchoolDetailsCallbackCaptor.value.onSchoolsDetailsListReceived(schoolsDetailsList)


        //progress bar is hidden
        assertFalse(LiveDataTestUtil.getValue(schoolsViewModel.mObsIsShowProgress))


        //to check selectedData observable is intialized if no data found
        assertNull(LiveDataTestUtil.getValue(schoolsViewModel.mObsSelectedSchoolDetails))



    }





}